# rooirobot


This repo contains various scripts to be executed by a bot. (The bot must make use of pywikibot to handle throttling, auth etc.)

It also includes files containing snippets of useful code for automating boring tasks. (generating files etc.)

Run `pwb login` in the venv console to login. It will prompt you for the password for the users listed in the user-config.py file.

Execute `python .venv/bin/pwb scripts/name_of_script.py` to run a specific script

## TODO

- Split generic functionality into it's own functions (looping over random pages, or looping over a list of page names)
