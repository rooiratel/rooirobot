import os

import pywikibot


def run(page_list, site):
    print("Running sp fix")
    debug = False

    change_text = 'genus'

    for pg in page_list:
        page = pywikibot.Page(site, pg)
        page_title = page.title()
        text = page.text

        if debug:
            print("teks is : \n" + text.__repr__())

        if not page.exists():
            print('Bladsy :' + page_title + ' bestaan nie.')
            continue
        else:
            print('Besig met : ' + page_title)

            # keep track of whether you are in the template or not
            paren_counter = 0
            left_templ = '{{'
            right_templ = '}}'

            # loop over every line in page text
            for line in text.splitlines():

                if left_templ in line:
                    paren_counter += 1

                if right_templ in line:
                    paren_counter -= 1

                if paren_counter == 0:
                    print("End of template. Breaking out of loop.")
                    break

                if change_text in line and "|" in line and "=" in line:
                    text = text.replace(line, '')


            page.text = text
            print("Klaar met sp_fix")

            if debug:
                print("teks is nou : \n" + page.text.__repr__())
            else:
                page.save('Spesieboks param', minor=False)
