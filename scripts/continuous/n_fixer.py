#!/usr/bin/env python
import pywikibot

site = pywikibot.Site("af", "wikipedia")

page_list = ["Gewasbeskerming", "Oogprobleme"]

bad_str = " ŉ "
good_str = " 'n "

for page in page_list:
    page = pywikibot.Page(site, page)
    page_title = page.title()
    text = page.text

    if not page.exists():
        print('Bladsy :' + page_title + ' bestaan nie.')
    else:
        # loop over every line in page text
        for line in text.splitlines():

            if bad_str in line:
                text = text.replace(bad_str, good_str)

        page.text = text  # indent?

        page.save(bad_str + " -> " + good_str + " | sien [[ʼn]]", minor=True)
        # page.save('Verander na Spesieboks', minor=True, botflag=True)
