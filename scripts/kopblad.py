import pywikibot


def run(page_list, site):
    print("Running kopblad.py")
    debug: bool = False

    for pg in page_list:
        blad = pywikibot.Page(site, pg)

        if blad.isRedirectPage():
            print(blad.title() + " is a redirect.")
            blad = blad.getRedirectTarget()
            print("following redirect to : ", blad.title())

        found = False
        while not found:

            # toggle talk
            if not blad.isTalkPage():
                blad = blad.toggleTalkPage()

            # check if exists
            bladlen = len(blad.text)
            if bladlen > 0:
                print(blad.title() + " has a non-empty talk page. Size = " + str(bladlen))
                found = True
            else:
                print(blad.title() + " is an empty talk page.")

        # end of while not found loop

        kop: str = '{{Kop van besprekingsbladsy}}'
        bladtrekke: str = '{{Bladtrekke}}'
        contains_kop: bool = False
        contains_bladtrekke: bool = False
        skip: bool = False
        save_msg: str = ''

        if kop in blad.text:
            contains_kop = True
        if bladtrekke in blad.text:
            contains_bladtrekke = True
        if contains_kop and contains_bladtrekke:
            print('already has Kop and Bladtrekke')
            skip = True
        elif contains_kop and not contains_bladtrekke:
            blad.text = blad.text.replace(kop, (kop + '\n' + bladtrekke + '\n'))
            save_msg = 'Bladtrekke'
        elif not contains_kop and contains_bladtrekke:
            blad.text = kop + '\n' + blad.text
            save_msg = 'Kop'
        elif not contains_kop and not contains_bladtrekke:
            blad.text = kop + '\n' + bladtrekke + '\n' + blad.text
            save_msg = 'Bladtrekke en Kop'

        if debug:
            print(save_msg)
        else:
            if not skip:
                try:
                    blad.save(save_msg)  # Saves the page
                except pywikibot.exceptions.EditConflictError:
                    pywikibot.output('Edit conflict! skip!')
                except pywikibot.exceptions.ServerError:
                    pywikibot.output('Server Error! Wait..')
                except pywikibot.exceptions.SpamblacklistError as e:
                    pywikibot.output('Cannot change {} because of blacklist entry {}'.format(blad.title(), e.url))
                except pywikibot.exceptions.LockedPageError:
                    pywikibot.output('Skipping {} (locked page)'.format(blad.title()))
                except pywikibot.exceptions.PageSaveRelatedError as error:
                    pywikibot.output('Error putting page: {}'.format(error.args))


