import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import datetime
from matplotlib.ticker import MultipleLocator
from scour.scour import scourString
from scour.scour import sanitizeOptions as sanitizeScourOptions
from scour.scour import parse_args as parseScourArgs


# TODO upload the final file to commons when done

# funcs
def optimize_svg(sourcesvg):
    # NOTE this could break at any time
    # SEE : https://github.com/scour-project/scour/issues/290
    scouroptions = parseScourArgs([
        "--enable-id-stripping",
        "--enable-comment-stripping",
        "--shorten-ids",
        "--indent=none",
        "--no-line-breaks"])
    scouroptions = sanitizeScourOptions(scouroptions)
    optimizedsvg = scourString(sourcesvg, scouroptions)
    return optimizedsvg


def run() -> None:
    # vars
    dt_format = '%Y-%m-%d'
    fat_file_name = './scripts/mylpale_fat.svg'
    thin_file_name = './scripts/mylpale.svg'

    # Initialize lists to store the dates and values
    dates = []
    values = []

    # TODO pull data from wikipedia page?
    # format of mylpale_data.txt is as follows
    # 2001-11-01 1
    # 2002-08-01 50
    # 2003-04-01 100
    # etc.

    # Current directory: /mnt/red6/code/rooirobot
    # Get the current working directory
    # current_dir = os.getcwd()
    # print(f"Current directory: {current_dir}")

    # Open the text file and read the data
    with open('./scripts/mylpale_data.txt', 'r') as file:
        for line in file:
            # Split the line into date and value
            date_str, value_str = line.strip().split()
            # Convert the date string to a datetime object
            date = datetime.strptime(date_str, dt_format)
            # Convert the value string to an integer
            value = int(value_str)
            # Append the date and value to their respective lists
            dates.append(date)
            values.append(value)

    # Create a figure and set the size
    fig, ax = plt.subplots(figsize=(12, 6))  # Adjust the width and height as needed

    # Plot the data
    ax.plot_date(dates, values, 'o-', markersize=5, linewidth=2, color='#0060ad')

    # Set the title and labels
    ax.set_title("Aantal Afrikaanse Wikipedia Artikels (18 Nov 2024)", fontsize=14)
    ax.set_xlabel("Datum")
    ax.set_ylabel("Aantal Artikels")

    # Set the x-axis major ticks to years
    ax.xaxis.set_major_locator(mdates.YearLocator())
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))

    # Set the x-axis range
    start_date = datetime.strptime('2001-06-01', dt_format)
    end_date = datetime.strptime(f"{datetime.now().year + 2}-01-01", '%Y-%m-%d')  # Use current year + 2 as the end date
    ax.set_xlim(start_date, end_date)

    # Set y-axis range
    ax.set_ylim(0, (values[-1] + 5000))

    # Set the y-axis gridlines to every 5000
    ax.yaxis.set_major_locator(MultipleLocator(5000))

    # Remove the x-axis and y-axis ticks
    ax.xaxis.set_tick_params(which='both', bottom=False, top=False, labelbottom=True)
    ax.yaxis.set_tick_params(which='both', left=False, right=False, labelleft=True)

    # Add a grid
    ax.grid(True)

    # Add padding to the plot
    ax.margins(x=0.1, y=0.1)  # Adjust x and y padding as needed

    # Save the plot as an SVG file
    plt.savefig(fat_file_name, format='svg', dpi=1200)

    print('Klaar met vet .svg')
    print('Besig om dit maar te maak.')

    # Step 1: Read the SVG file as a string
    with open(fat_file_name, 'r') as file:
        svg_xml_string = file.read()

    # Step 2: Optimise the XML string
    optimized_svg_xml = optimize_svg(svg_xml_string)

    # Step 3: Save the optimized string as a new SVG file
    with open(thin_file_name, 'w') as file:
        file.write(optimized_svg_xml)

    print('Klaar')
