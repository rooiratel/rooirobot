import pywikibot


def create_wikidata_item(label_dict, taxon_name, parent_taxon_qid, powo_id, ipni_id, gbif_id, inat_id):
    """Creates a new Wikidata item with given labels."""
    site = pywikibot.Site("wikidata", "wikidata")
    repo = site.data_repository()

    # Create a new item
    print('Setting labels')
    new_item = pywikibot.ItemPage(repo)
    new_item.editLabels(label_dict, summary="Setting labels")
    new_item.editDescriptions({"af": "plantspesie", "en": "plant species"}, summary="Setting descriptions")

    # Add properties
    print('Adding instance of taxon')
    taxon_claim = pywikibot.Claim(repo, 'P31')  # Instance of (P31)
    target = pywikibot.ItemPage(repo, 'Q16521')  # taxon (Q16521)
    taxon_claim.setTarget(target)
    new_item.addClaim(taxon_claim, summary='Adding instance of taxon')

    print('adding taxon name')
    taxon_name_claim = pywikibot.Claim(repo, u'P225')  # Taxon name (P225)
    taxon_name_claim.setTarget(taxon_name)  # Using a string # taxon name value 'genus species'
    new_item.addClaim(taxon_name_claim, summary='Adding taxon name')

    print('adding taxon rank')
    taxon_rank_claim = pywikibot.Claim(repo, 'P105')  # Taxon rank (P105)
    target = pywikibot.ItemPage(repo, 'Q7432')  # species (Q7432) # todo add other taxon ranks
    taxon_rank_claim.setTarget(target)
    new_item.addClaim(taxon_rank_claim, summary='Adding taxon rank')

    if parent_taxon_qid:
        print('adding parent taxon')
        parent_taxon_claim = pywikibot.Claim(repo, 'P171')  # Parent taxon (P171)
        target = pywikibot.ItemPage(repo, parent_taxon_qid)
        parent_taxon_claim.setTarget(target)
        new_item.addClaim(parent_taxon_claim, summary='Adding parent taxon')

    if powo_id:
        print('adding POWO ID')
        powo_claim = pywikibot.Claim(repo, 'P5037')  # POWO ID (P5037)
        powo_claim.setTarget(powo_id)
        new_item.addClaim(powo_claim, summary='Adding POWO ID')

    if ipni_id:
        print('adding IPNI ID')
        ipni_claim = pywikibot.Claim(repo, 'P961')  # IPNI ID (P961)
        ipni_claim.setTarget(ipni_id)
        new_item.addClaim(ipni_claim, summary='Adding IPNI ID')

    if gbif_id:
        print('adding GBIF ID')
        gbif_claim = pywikibot.Claim(repo, 'P846')  # GBIF ID (P846)
        gbif_claim.setTarget(gbif_id)
        new_item.addClaim(gbif_claim, summary='Adding GBIF ID')

    if inat_id:
        print('adding inat ID')
        inat_claim = pywikibot.Claim(repo, 'P3151')  # iNat ID (P3151)
        inat_claim.setTarget(inat_id)
        new_item.addClaim(inat_claim, summary='Adding iNat ID')

    # Add a sitelink to the new item
    print("adding afwiki sitelink")
    site = pywikibot.Site("af", "wikipedia")
    page = pywikibot.Page(site, taxon_name)
    new_item.setSitelink(page)

    return new_item.getID()


def run(taxon_name, parent_taxon_qid, ipni_id, gbif_id, inat_id):
    labels = {
        "af": "Will be set to taxon_name in loop below",
        "en": "Will be set to taxon_name in loop below"
    }

    for key in labels.keys():
        labels[key] = taxon_name

    powo_id = 'urn:lsid:ipni.org:names:' + ipni_id
    # gbif_id = ''
    # inat_id = ''

    new_item_id = create_wikidata_item(labels, taxon_name, parent_taxon_qid, powo_id, ipni_id, gbif_id, inat_id)
    print(f"Created new item with ID: {new_item_id}")
