import pywikibot
import re


def is_wees_bladsy(page):
    # Check if the page exists and is in the Main namespace
    if not page.exists() or page.namespace() != 0:
        return False

    # Check if the page is a disambiguation page
    if page.isDisambig():
        return False

    # Get all references to the page
    refs = page.getReferences(follow_redirects=True, with_template_inclusion=False)

    # Filter out references that are not in the Main namespace or are talk pages
    filtered_refs = [
        ref for ref in refs
        if ref.namespace() == 0 and not ref.isTalkPage()
    ]

    # If there are no remaining references, the page is an orphan
    return len(filtered_refs) == 0


def run(page_list, site):
    debug = False

    for pg in page_list:
        page = pywikibot.Page(site, pg)

        page_title = page.title()
        text = page.text

        # Regex pattern to match the template
        pattern = r'\{\{[Ww]eesbladsy(?:\|[dD]atum=[^\}]*)?\}\}'

        # Check if the template is present in the text
        het_sjabloon = re.search(pattern, text)

        print(f"Besig om wees status na te gaan vir {page_title}")

        if not page.exists():
            print(f'Bladsy : {page_title} bestaan nie.')
        else:

            if page.isRedirectPage():
                print(f"{page_title} is a redirect.")
                page = page.getRedirectTarget()
                print(f"following redirect to : {page_title}")

            # Kyk of die bladsy wees is
            is_wees = is_wees_bladsy(page)

            if is_wees:
                print('Die bladsy is wees')
                # insert tag if needed
                if het_sjabloon:
                    continue
                else:
                    # Insert the template at top of the page
                    page.text = "{{Weesbladsy}}\n" + text
            else:
                print('Die bladsy is nie wees nie')
                # remove tag if needed
                if het_sjabloon:
                    # Replace the template with an empty string
                    new_text = re.sub(pattern, '', text)
                    page.text = new_text
                else:
                    continue

                print("Klaar met wees status nagaan vir " + page_title)

            if debug:
                print("teks is nou : \n" + page.text.__repr__())
            else:
                if is_wees:
                    page.save('Bladsy is wees', minor=True)
                else:
                    page.save('Bladsy is nie wees nie', minor=True)

        print("\n")
