import pywikibot


def run(site, bladsy_naam, beeld_naam, beeld_onderskrif):
    print("besig om voeg_beeld_by te loop")
    debug = False

    bladsy = pywikibot.Page(site, bladsy_naam)

    if bladsy.isRedirectPage():
        print(bladsy.title() + " is a redirect.")
        bladsy = bladsy.getRedirectTarget()
        print("following redirect to : ", bladsy.title())

    page_title = bladsy.title()
    text = bladsy.text

    if debug:
        print("teks is : \n" + text.__repr__())

    if not bladsy.exists():
        print('Bladsy :' + page_title + ' bestaan nie.')
    else:
        print('Besig met : ' + page_title)
        text = "[[Lêer:" + beeld_naam + "|duimnael|regs|" + beeld_onderskrif + "]]\n" + text

        # remove first and last blank lines
        text = text.strip('\n')

        # remove middle blank lines
        while '\n\n\n' in text:
            text = text.replace('\n\n\n', '\n\n')

        bladsy.text = text

    if debug:
        print("teks is nou : \n" + bladsy.text.__repr__())
    else:
        bladsy.save('Beeld bygevoeg', minor=False)
