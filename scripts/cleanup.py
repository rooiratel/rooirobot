import os

import pywikibot


def run(page_list, site):
    print("Running cleanup task")
    debug = False

    for pg in page_list:
        page = pywikibot.Page(site, pg)
        page_title = page.title()
        text = page.text

        if debug:
            print("teks is : \n" + text.__repr__())

        if not page.exists():
            print('Bladsy :' + page_title + ' bestaan nie.')
            break
        else:
            print('Besig met : ' + page_title)

            print("Soek vir lee lyne")

            # remove first and last blank lines
            text = text.strip('\n')

            # remove middle blank lines
            while '\n\n\n' in text:
                text = text.replace('\n\n\n', '\n\n')

            page.text = text
            print("Klaar met opruim vir " + page_title)

            if debug:
                print("teks is nou : \n" + page.text.__repr__())
            else:
                page.save('Opruim', minor=False)
