#!/usr/bin/env python
import pywikibot


def is_bad_line(current_line):
    bad_strs = ['regnum', 'unranked_divisio', 'unranked_classis', 'unranked_ordo', 'divisio', 'classis',
                'ordo', 'familia', 'phylum', 'image_width', 'image2_width', 'subfamilia', 'tribus', 'subtribus',
                'species_authority', 'worms', 'koninkryk', 'subkoninkryk', 'filum', 'klas', 'orde', 'familie',
                'titelweergawe']

    good_strs = ['subspesies']

    has_bad_str = False

    # if speciesboks contains taxon parameter, then remove genus and species params, otherwise leave them
    for bstr in bad_strs:
        if bstr in current_line and '=' in current_line and current_line.lower() not in good_strs:
            has_bad_str = True
            break

    return has_bad_str


##########################################
def run(page_list, site):
    print("Running change_to_spesieboks")
    debug = False

    change_text0 = '{{taksoboks'
    change_text0b = '{{taxobox'

    change_text1 = 'binomial'
    change_text2 = 'binomial_authority'
    change_text2b = 'taxon_authority'

    change_text3 = 'name'
    change_text4 = '|image ='
    change_text5 = '|image_caption ='
    change_text6 = '|status ='
    change_text7 = '|status_system ='

    change_text8 = 'naam'
    change_text9 = 'beeld'
    change_text10 = 'beeldonderskrif'
    change_text11 = 'subdivisie'
    change_text12 = 'w-name'
    change_text12b = 'w-naam'
    change_text13 = 'outeur'
    change_text14 = 'synoniem'

    for pg in page_list:
        page = pywikibot.Page(site, pg)
        page_title = page.title()
        text = page.text.strip()

        if debug:
            print("teks is : \n" + text.__repr__())

        if not page.exists():
            print('Bladsy :' + page_title + ' bestaan nie.')
            continue
        else:
            print('Besig met : ' + page_title)

            # remove empty line at beginning

            # keep track of whether you are in the template or not
            paren_counter = 0
            left_templ = '{{'
            right_templ = '}}'

            lines = text.splitlines()
            if lines[0].strip() == '':
                print("First line is empty. Skipping it.")
                lines = lines[1:]

            # loop over every line in page text
            for line in lines:

                if '{{skuinse titel}}' in line.lower():
                    text = text.replace(line, '')
                    continue

                if left_templ in line:
                    paren_counter += 1

                if right_templ in line:
                    paren_counter -= 1

                if paren_counter == 0:
                    print("End of template. Breaking out of loop.")
                    break

                # print("change_text0 : " + change_text0)
                # print("line.lower() : " + line.lower())
                if change_text0 in line.lower():
                    text = text.replace(line, '{{Spesieboks')

                if change_text0b in line.lower():
                    text = text.replace(line, '{{Spesieboks')

                # remove unnecessary lines
                if is_bad_line(line):
                    text = text.replace(line, '').rstrip('\n')  # blank line

                # fok tog, die was nou 'n gesukkel
                if change_text1 in line and "|" in line and "=" in line and change_text2 not in line:
                    clean_line = line.replace("'", "")
                    text = text.replace(line, clean_line)
                    text = text.replace(clean_line, clean_line.replace(change_text1, 'taxon'))

                # optimise this crap below
                if change_text2 in line and "|" in line and "=" in line:
                    text = text.replace(change_text2, 'authority')

                if change_text2b in line and "|" in line and "=" in line:
                    print('line is : ' + line)
                    text = text.replace(change_text2b, 'authority')

                if change_text3 in line and "|" in line and "=" in line:
                    text = text.replace(change_text3, 'name')

                if change_text4 in line:
                    text = text.replace(change_text4, '| image =')

                if change_text5 in line:
                    text = text.replace(change_text5, '| image_caption =')

                if change_text6 in line:
                    text = text.replace(change_text6, '| status =')

                if change_text7 in line:
                    text = text.replace(change_text7, '| status_system =')

                if change_text8 in line and "|" in line and "=" in line and change_text12b not in line and '-->' not in line:
                    text = text.replace(line, line.replace(change_text8, 'name'))

                if change_text9 in line and "|" in line and "=" in line and change_text10 not in line:
                    text = text.replace(line, line.replace(change_text9, 'image'))

                if change_text10 in line and "|" in line and "=" in line:
                    text = text.replace(line, line.replace(change_text10, 'image_caption'))

                if change_text11 in line and "|" in line and "=" in line:
                    text = text.replace(line, line.replace(change_text11, 'subdivision'))

                if change_text12 in line and "|" in line and "=" in line:
                    clean_line = line.replace("'", "")  # create a function for this
                    text = text.replace(line, clean_line)
                    text = text.replace(clean_line, clean_line.replace(change_text12, 'taxon'))

                if change_text12b in line and "|" in line and "=" in line:
                    clean_line = line.replace("'", "")  # create a function for this
                    text = text.replace(line, clean_line)
                    text = text.replace(clean_line, clean_line.replace(change_text12b, 'taxon'))

                if change_text13 in line and "|" in line and "=" in line:
                    text = text.replace(line, line.replace(change_text13, 'authority'))

                if change_text14 in line and "|" in line and "=" in line:
                    text = text.replace(line, line.replace(change_text14, 'synonyms'))

            # remove blank lines
            while '\n\n|' in text:
                text = text.replace('\n\n|', '\n|')

            while '\n\n}}' in text:
                text = text.replace('\n\n}}', '\n}}')

            page.text = text
            print('Klaar met verander na Spesieboks')

            if debug:
                print(page.text)
            else:
                page.save('Verander na Spesieboks', minor=False)
