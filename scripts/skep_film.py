import pywikibot
import httpx
from string import Template
from datetime import date
from datetime import datetime
from bs4 import BeautifulSoup

bs_parser = 'html.parser'


# TODO : save the list of actors, then check if each page is still an orphan and fix

def run(site, rt_url_postfix, imdb_url_id, stc_url_id, movie_title, rolprent_suffix: bool):
    print("Running skep_film")

    page_title = movie_title  # todo add flag for adding (rolprent) suffix in title only

    if rolprent_suffix:
        page_title = page_title + ' (rolprent)'

    rt_url = 'https://www.rottentomatoes.com/m/' + rt_url_postfix
    imdb_url = 'https://www.imdb.com/title/' + imdb_url_id + '/'
    stc_url = 'https://www.soundtrackcollector.com/title/' + stc_url_id + '/'
    imdb_full_credits_url = imdb_url + 'fullcredits'  # for things like music_writer
    # imdb_company_credits_url = imdb_url + 'companycredits'  # for things like studios/distributors

    # user_agent_str = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'
    user_agent_str = "Bezos' ballsack"
    headers = {'User-Agent': user_agent_str, 'Cache-Control': 'no-cache', 'Accept-Language': 'en-US'}

    with httpx.Client() as client:
        response = client.get(imdb_url, headers=headers)  # get 403 response without useragent
        full_credits_response = client.get(imdb_full_credits_url, headers=headers)  # get 403 response without useragent

    soup = BeautifulSoup(response.content, bs_parser)
    full_credits_soup = BeautifulSoup(full_credits_response.content, bs_parser)

    # populate placeholder values
    today = date.today()
    current_date = translate_months(today.strftime("%-d %B %Y"))

    directors = ''
    writers = ''
    # director_upper_div = soup.select_one('div.sc-69e49b85-3.dIOekc')
    # presentation_div = director_upper_div.select_one("div", {"role": "presentation"})
    # presentation_div = director_upper_ul.select_one("div", {"role": "presentation"})
    # main_ul = presentation_div.select_one('ul')

    main_ul = soup.select_one('ul.jZUbvq')

    lis = main_ul.find_all(lambda tag: tag.name == 'li' and tag.parent == main_ul)  # only select top level li elements

    directors_li = lis[0]
    dir_div1 = directors_li.find('div')
    dir_ul1 = dir_div1.find('ul')
    directors_lis = dir_ul1.find_all('li')

    for director_li in directors_lis:
        a = director_li.find('a')
        directors = directors + '[[' + a.text + ']] <br />'

    directors = remove_br_from_end_of_str(directors)

    writers_li = lis[1]
    wri_div1 = writers_li.find('div')
    wri_ul1 = wri_div1.find('ul')
    writers_lis = wri_ul1.find_all('li')

    for writer_li in writers_lis:
        a = writer_li.find('a')
        writers = writers + '[[' + a.text + ']] <br />'

    writers = remove_br_from_end_of_str(writers)

    music_writers = get_music_writers(full_credits_soup)
    studios = get_studios(soup)
    distributors = ''  # todo get from /companycredits
    release_date = get_release_date(soup)
    runtime = get_runtime(soup)
    country_of_origin = get_origin(soup)
    language = get_lang(soup)
    budget = get_budget(soup)
    gross_revenue = get_gross_rev(soup)

    intro = "'''''" + page_title + "''''' is 'n rolprent deur " + directors.replace('<br />',
                                                                                    ', ') + " wat op " + release_date + ' vrygestel is.<ref name="imdb" /><ref name="rt" />'

    actor_list = get_actor_list(soup)

    imdb_id = imdb_url.split('/')[4]

    page = pywikibot.Page(site, page_title)

    if page.exists():
        print('\n' + page_title + ' bestaan reeds.\n')
        quit()
    else:
        print("Besig om " + page_title + " te skep")

        # Define a template string with a placeholder
        template = Template('''{{Inligtingskas rolprent
| naam              = $title
| beeld             = 
| regisseur         = $directors
| produksieleier    = 
| geskryf_deur      = $writers
| verteller         = 
| met               = 
| musiek            = $music_writers
| kinematografie    = 
| redigeerder       = 
| ateljee           = $studios
| verspreider       = $distributors
| vrygestel         = $release_date
| speeltyd          = $runtime
| land              = $country_of_origin
| taal              = $language
| begroting         = $budget
| bruto             = $gross_revenue
| voorafgegaan_deur = 
| opgevolg_deur     = 
| webtuiste         = 
| imdb_id           = $imdb_id
}}

$intro

<!-- == Samevatting == -->

== Rolverdeling ==
{{Div col|colwidth=20em}}
$actor_list
{{div col end}}

== Musiek ==
Die musiek vir die film is deur $music_writers.<ref name="stc" />

== Verwysings ==
{{Verwysings|verwysings=
<ref name="imdb">{{cite web |url = $imdb_url |title = $title |work = [[IMDb]] |publisher=[[Amazon.com|Amazon]] |access-date = $current_date }}</ref>

<ref name="rt">{{cite web |url = $rt_url |title = $title |work = [[Rotten Tomatoes]] |publisher=[[Fandango Media]] |access-date = $current_date }}</ref>

<ref name="stc">{{cite web |url = $stc_url |title = $title |publisher=SoundtrackCollector.com |access-date = $current_date }}</ref>
}}

== Eksterne skakels ==
* {{IMDb title|$imdb_id|id=$imdb_id}}
* {{Amptelike URL}}

{{Normdata}}

[[Kategorie:Rolprente]]''')

        # Create a dictionary with the values to substitute
        values = {'title': movie_title, 'directors': directors, 'writers': writers, 'music_writers': music_writers,
                  'studios': studios, 'distributors': distributors, 'release_date': release_date, 'runtime': runtime,
                  'country_of_origin': country_of_origin, 'language': language, 'budget': budget,
                  'gross_revenue': gross_revenue, 'imdb_id': imdb_id, 'intro': intro, 'actor_list': actor_list,
                  'imdb_url': imdb_url, 'rt_url': rt_url, 'stc_url': stc_url, 'current_date': current_date}

        # Use the substitute method to replace the placeholders with actual values
        filled_template = template.substitute(values)

        page.text = filled_template
        # print(filled_template)

        page.save('Nuwe bladsy geskep - ' + page_title, minor=False)


def remove_last_occurrence(string, substring):
    # Find the last occurrence of the substring
    index = string.rfind(substring)

    # If the substring is found
    if index != -1:
        # Remove the last occurrence of the substring
        return string[:index] + string[index + len(substring):]
    else:
        # If the substring is not found, return the original string
        return string


# If the substr

def get_studios(soup):
    studios_text = ''
    main_div = soup.find(attrs={"data-testid": "title-details-companies"})
    inner_div = main_div.find('div')
    ul = inner_div.find('ul')
    studio_lis = ul.find_all('li')

    for studio_li in studio_lis:
        a = studio_li.find('a')
        studios_text = studios_text + '[[' + a.text + ']] <br />'

    return remove_br_from_end_of_str(studios_text)


def get_release_date(soup):
    release_date_text = ''
    main_div = soup.find(attrs={"data-testid": "title-details-releasedate"})
    inner_div = main_div.find('div')
    ul = inner_div.find('ul')
    rel_date_lis = ul.find_all('li')

    for rel_date_li in rel_date_lis:
        a = rel_date_li.find('a')
        release_date_text = release_date_text + a.text

    release_date_text = release_date_text.split('(')[0]

    # now convert from shite US format to normal format
    # Assume 'american_date' is a string representing a date in American format
    american_date = release_date_text.strip()  # e.g. : October 3, 2014

    # Convert the American date to a datetime object
    date_object = datetime.strptime(american_date, '%B %d, %Y')

    # Convert the datetime object to a string in international format
    international_date = date_object.strftime('%-d %B %Y')

    return translate_months(international_date)


def get_origin(soup):
    coo_text = ''
    main_div = soup.find(attrs={"data-testid": "title-details-origin"})
    inner_div = main_div.find('div')
    ul = inner_div.find('ul')
    coo_lis = ul.find_all('li')

    for coo_li in coo_lis:
        a = coo_li.find('a')
        coo_text = coo_text + '[[' + a.text + ']] <br />'

    coo_text = remove_br_from_end_of_str(coo_text)

    return translate_country(coo_text)


def get_lang(soup):
    lang_text = ''
    main_div = soup.find(attrs={"data-testid": "title-details-languages"})
    inner_div = main_div.find('div')
    ul = inner_div.find('ul')
    lang_lis = ul.find_all('li')

    for lang_li in lang_lis:
        a = lang_li.find('a')
        lang_text = lang_text + '[[' + a.text + ']] <br />'

    lang_text = remove_br_from_end_of_str(lang_text)

    return translate_langs(lang_text)


def get_runtime(soup):
    runtime_text = ''
    main_div = soup.find(attrs={"data-testid": "title-techspec_runtime"})
    inner_div = main_div.find('div')
    runtime_text = runtime_text + inner_div.text
    runtime_text = runtime_text.replace('hours', 'ure').replace('hour', 'uur').replace('minutes', 'minute')

    return runtime_text


def get_budget(soup):
    budget_text = ''
    main_div = soup.find(attrs={"data-testid": "title-boxoffice-budget"})

    if main_div is not None:
        inner_div = main_div.find('div')

        if inner_div is not None:
            ul = inner_div.find('ul')
            budget_li = ul.find('li')  # only get the one budget
            budget_span = budget_li.find('span')
            budget_text = budget_text + budget_span.text
            budget_text = budget_text.replace('$', '[[VSA dollar|VS$]] ').replace(' (estimated)', '')

    return budget_text


def get_gross_rev(soup):
    grev_text = ''
    main_div = soup.find(attrs={"data-testid": "title-boxoffice-cumulativeworldwidegross"})
    inner_div = main_div.find('div')

    ul = inner_div.find('ul')
    grev_li = ul.find('li')  # only get the one budget
    grev_span = grev_li.find('span')
    grev_text = grev_text + grev_span.text
    grev_text = grev_text.replace('$', '[[VSA dollar|VS$]] ').replace(' (estimated)', '')

    return grev_text


def get_actor_list(soup):
    actor_list_text = ''
    main_section = soup.select_one(
        'section.ipc-page-section.ipc-page-section--base.sc-bfec09a1-0.dGyVLT.title-cast.title-cast--movie.celwidget')

    main_div = main_section.find(attrs={"data-testid": "shoveler-items-container"})
    actor_divs = main_div.select('div.sc-bfec09a1-5.hNfYaW')

    for actor_div in actor_divs:
        name_div = actor_div.select_one('div.sc-bfec09a1-7')

        a_element = name_div.select_one('a')

        actor_name = a_element.text

        span = actor_div.select_one('span.sc-bfec09a1-4.kvTUwN')
        char_name = span.text
        actor_list_text = actor_list_text + '* [[' + actor_name + ']] as ' + char_name + '\n'

    return actor_list_text


def get_music_writers(soup):
    music_writer_text = ''
    main_div = soup.find('div', id='fullcredits_content')

    composer_h4 = main_div.find('h4', id='composer')
    # Check if composer_h4 was found
    if composer_h4 is not None:
        target_table = composer_h4.find_next_sibling('table')
        # Example of accessing the table content
        for row in target_table.find_all('tr'):
            a_text = row.find('a').text
            music_writer_text = music_writer_text + a_text + ', '
    else:
        print("The 'composer' h4 element was not found within the target div.")

    return remove_last_occurrence(music_writer_text.strip(), ',')


def remove_br_from_end_of_str(str_to_clean):
    substring = ' <br />'

    # Split the string using the substring as the delimiter
    parts = str_to_clean.rsplit(substring, 1)

    # Join the parts back together, omitting the substring
    return ''.join(parts)  # removes the <br /> from the end of the string


def translate_months(date_string):
    months = {
        'January': 'Januarie',
        'February': 'Februarie',
        'March': 'Maart',
        'April': 'April',
        'May': 'Mei',
        'June': 'Junie',
        'July': 'Julie',
        'August': 'Augustus',
        'September': 'September',
        'October': 'Oktober',
        'November': 'November',
        'December': 'Desember'
    }

    for english_month, afrikaans_month in months.items():
        date_string = date_string.replace(english_month, afrikaans_month)

    return date_string


def translate_country(country_string):
    countries = {
        'USA': 'Verenigde State van Amerika',
        'United States': 'Verenigde State van Amerika',
        'UK': 'Verenigde Koninkryk',
        'United Kingdom': 'Verenigde Koninkryk',
        'France': 'Frankryk',
        'Germany': 'Duitsland',
        'Italy': 'Italië',
        'Japan': 'Japan',
        'India': 'Indië',
        'China': 'Sjina',
        'Russia': 'Rusland',
        'Australia': 'Australië',
        'Canada': 'Kanada',
        'Spain': 'Spanje',
        'Argentina': 'Argentinië',
        'Mexico': 'Meksiko',
        'Brazil': 'Brasilië',
        'Ireland': 'Ierland',
        'South Africa': 'Suid-Afrika',
        'Netherlands': 'Nederland',
        'Sweden': 'Swede',
        'Denmark': 'Denemarke',
        'Poland': 'Pole',
        'Norway': 'Noorweë',
        'Finland': 'Finland',
        'New Zealand': 'Nieu-Seeland'
    }

    for english_country, afrikaans_country in countries.items():
        country_string = country_string.replace(english_country, afrikaans_country)

    return country_string


def translate_langs(language_string):
    languages = {
        'Afrikaans': 'Afrikaans',
        'English': 'Engels',
        'Spanish': 'Spaans',
        'French': 'Frans',
        'Italian': 'Italiaans',
        'German': 'Duits',
        'Japanese': 'Japannees',
        'Chinese': 'Sjinees',
        'Hindi': 'Hindi',
        'Russian': 'Russisch',
        'Arabic': 'Arabies',
        'Bengali': 'Bengaals',
        'Portuguese': 'Portugees',
        'Malayalam': 'Maleis',
        'Marathi': 'Marathi',
        'Telugu': 'Teloegoe',
        'Tamil': 'Tamil',
        'Kannada': 'Kannada',
        'Thai': 'Thais',
        'Vietnamese': 'Vietnamees',
        'Indonesian': 'Indonees',
        'Polish': 'Poolse',
        'Dutch': 'Nederlands'
    }

    for english_language, afrikaans_language in languages.items():
        language_string = language_string.replace(english_language, afrikaans_language)

    return language_string
