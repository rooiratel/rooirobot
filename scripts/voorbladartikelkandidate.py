#!/usr/bin/env python
import pywikibot

min_size = 25000  # minimum page size in bytes
max_size = 35000  # maximum page size in bytes


def page_in_size_range(page_size):
    return min_size <= page_size <= max_size


def run(site):
    print("Running voorbladartikelkandidate")

    # overwrite file if it already exists
    filename = "voorbladartikelkandidate.csv"
    with open(filename, 'w') as file:
        file.write('title,size,prose_size\n')  # csv header line

    all_pages = site.allpages()

    for page in all_pages:

        page_title = page.title()
        page_size = list(page.revisions(total=1))[0].size  # more memory efficient than len(page.text)
        page_prose_size = 0

        if page_in_size_range(page_size):
            # todo get prose size

            # append to the file
            with open(filename, "a") as f:
                f.write(page_title + ',' + str(page_size) + ',' + str(page_prose_size) + '\n')
        else:
            print(page_title + ' size [' + str(page_size) + '] not in range')
