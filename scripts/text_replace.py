import pywikibot


def run(page_list, site, old_text, new_text):
    print("Running text replace")
    debug = False

    for pg in page_list:
        page = pywikibot.Page(site, pg)

        if page.isRedirectPage():
            print(page.title() + " is a redirect.")
            page = page.getRedirectTarget()
            print("following redirect to : ", page.title())

        page_title = page.title()
        text = page.text

        if debug:
            print("teks is : \n" + text.__repr__())

        if not page.exists():
            print('Bladsy :' + page_title + ' bestaan nie.')
            continue
        else:
            print('Besig met : ' + page_title)

            print("Vervang [ " + old_text + " ] met [ " + new_text + " ]")
            text = text.replace(old_text, new_text)

            # remove first and last blank lines
            text = text.strip('\n')

            # remove middle blank lines
            while '\n\n\n' in text:
                text = text.replace('\n\n\n', '\n\n')

            page.text = text
            print("Klaar met text vervanging " + page_title)

            if debug:
                print("teks is nou : \n" + page.text.__repr__())
            else:
                 page.save('Verander [ ' + old_text + ' ] na [ ' + new_text + ' ]', minor=True)
                # page.save('Interne skakels', minor=True)
