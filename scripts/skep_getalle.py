from string import Template

import pywikibot
import math


def is_even_number(n):
    return n % 2 == 0


def is_prime_number(n):
    if n <= 1:
        return False

    # Check if n is divisible by any number up to sqrt(n)
    for i in range(2, int(math.sqrt(n)) + 1):
        if n % i == 0:
            return False

    return True


def format_float(num):
    formatted = "{:.3f}".format(num)

    # Remove trailing zeros if present
    if formatted.endswith(".000"):
        return formatted.split('.')[0]
    else:
        return formatted


def getCalcString(num):
    output = '''=== Lys van basiese berekeninge ===
{| class ="wikitable" style="text-align: center;"
|-
!width = "105px" | [[Vermenigvuldiging]]
!1
!2
!3
!4
!5
!6
!7
!8
!9
!10
!11
!12
!13
!14
!15
!16
!17
!18
!19
!20
!21
!22
!23
!24
!25
!50
!100
!1000
!10000
|-
| ''' + str(num) + ''' × ''x''\'''
| [[''' + str(num * 1) + ''' (getal) | ''' + str(num * 1) + ''']]
| [[''' + str(num * 2) + ''' (getal) | ''' + str(num * 2) + ''']]
| [[''' + str(num * 3) + ''' (getal) | ''' + str(num * 3) + ''']]
| [[''' + str(num * 4) + ''' (getal) | ''' + str(num * 4) + ''']]
| [[''' + str(num * 5) + ''' (getal) | ''' + str(num * 5) + ''']]
| [[''' + str(num * 6) + ''' (getal) | ''' + str(num * 6) + ''']]
| [[''' + str(num * 7) + ''' (getal) | ''' + str(num * 7) + ''']]
| [[''' + str(num * 8) + ''' (getal) | ''' + str(num * 8) + ''']]
| [[''' + str(num * 9) + ''' (getal) | ''' + str(num * 9) + ''']]
| [[''' + str(num * 10) + ''' (getal) | ''' + str(num * 10) + ''']]
| [[''' + str(num * 11) + ''' (getal) | ''' + str(num * 11) + ''']]
| [[''' + str(num * 12) + ''' (getal) | ''' + str(num * 12) + ''']]
| [[''' + str(num * 13) + ''' (getal) | ''' + str(num * 13) + ''']]
| [[''' + str(num * 14) + ''' (getal) | ''' + str(num * 14) + ''']]
| [[''' + str(num * 15) + ''' (getal) | ''' + str(num * 15) + ''']]
| [[''' + str(num * 16) + ''' (getal) | ''' + str(num * 16) + ''']]
| [[''' + str(num * 17) + ''' (getal) | ''' + str(num * 17) + ''']]
| [[''' + str(num * 18) + ''' (getal) | ''' + str(num * 18) + ''']]
| [[''' + str(num * 19) + ''' (getal) | ''' + str(num * 19) + ''']]
| [[''' + str(num * 20) + ''' (getal) | ''' + str(num * 20) + ''']]
| [[''' + str(num * 21) + ''' (getal) | ''' + str(num * 21) + ''']]
| [[''' + str(num * 22) + ''' (getal) | ''' + str(num * 22) + ''']]
| [[''' + str(num * 23) + ''' (getal) | ''' + str(num * 23) + ''']]
| [[''' + str(num * 24) + ''' (getal) | ''' + str(num * 24) + ''']]
| [[''' + str(num * 25) + ''' (getal) | ''' + str(num * 25) + ''']]
| [[''' + str(num * 50) + ''' (getal) | ''' + str(num * 50) + ''']]
| [[''' + str(num * 100) + ''' (getal) | ''' + str(num * 100) + ''']]
| [[''' + str(num * 1000) + ''' (getal) | ''' + str(num * 1000) + ''']]
| [[''' + str(num * 10000) + ''' (getal) | ''' + str(num * 10000) + ''']]
|}

{|class ="wikitable" style="text-align: center;"
|-
! [[Deling]]
!1
!2
!3
!4
!5
!6
!7
!8
!9
!10
!width = "5px" |
!11
!12
!13
!14
!15
!16
!17
!18
!19
!20
|-
| ''' + str(num) + ''' ÷ ''x''\'''
| ''' + str(format_float(num / 1)) + '''
| ''' + str(format_float(num / 2)) + '''
| ''' + str(format_float(num / 3)) + '''
| ''' + str(format_float(num / 4)) + '''
| ''' + str(format_float(num / 5)) + '''
| ''' + str(format_float(num / 6)) + '''
| ''' + str(format_float(num / 7)) + '''
| ''' + str(format_float(num / 8)) + ''' 
| ''' + str(format_float(num / 9)) + ''' 
| ''' + str(format_float(num / 10)) + '''
! 
| ''' + str(format_float(num / 11)) + ''' 
| ''' + str(format_float(num / 12)) + ''' 
| ''' + str(format_float(num / 13)) + ''' 
| ''' + str(format_float(num / 14)) + ''' 
| ''' + str(format_float(num / 15)) + ''' 
| ''' + str(format_float(num / 16)) + ''' 
| ''' + str(format_float(num / 17)) + ''' 
| ''' + str(format_float(num / 18)) + ''' 
| ''' + str(format_float(num / 19)) + ''' 
| ''' + str(format_float(num / 20)) + '''
|-
| x ÷ ''' + str(num) + '''
| ''' + str(format_float(1 / num)) + ''' 
| ''' + str(format_float(2 / num)) + ''' 
| ''' + str(format_float(3 / num)) + ''' 
| ''' + str(format_float(4 / num)) + ''' 
| ''' + str(format_float(5 / num)) + ''' 
| ''' + str(format_float(6 / num)) + ''' 
| ''' + str(format_float(7 / num)) + ''' 
| ''' + str(format_float(8 / num)) + ''' 
| ''' + str(format_float(9 / num)) + ''' 
| ''' + str(format_float(10 / num)) + '''
!
| ''' + str(format_float(11 / num)) + '''
| ''' + str(format_float(12 / num)) + '''
| ''' + str(format_float(13 / num)) + '''
| ''' + str(format_float(14 / num)) + '''
| ''' + str(format_float(15 / num)) + '''
| ''' + str(format_float(16 / num)) + ''' 
| ''' + str(format_float(17 / num)) + ''' 
| ''' + str(format_float(18 / num)) + ''' 
| ''' + str(format_float(19 / num)) + ''' 
| ''' + str(format_float(20 / num)) + ''' 
|}

{| class ="wikitable" style="text-align: center;"
|-
! [[Magsverheffing]]
!1
!2
!3
!4
!5
!6
!7
!8
!9
!10
!width = "5px" |
!11
!12
!13
!14
!15
!16
!17
!18
!19
!20
|-
| ''' + str(num) + '''<sup>x</sup>
| ''' + str(format_float(num ** 1)) + '''
| ''' + str(format_float(num ** 2)) + '''
| ''' + str(format_float(num ** 3)) + '''
| ''' + str(format_float(num ** 4)) + '''
| ''' + str(format_float(num ** 5)) + '''
| ''' + str(format_float(num ** 6)) + '''
| ''' + str(format_float(num ** 7)) + '''
| ''' + str(format_float(num ** 8)) + '''
| ''' + str(format_float(num ** 9)) + '''
| ''' + str(format_float(num ** 10)) + '''
!
| ''' + str(format_float(num ** 11)) + '''
| ''' + str(format_float(num ** 12)) + '''
| ''' + str(format_float(num ** 13)) + '''
| ''' + str(format_float(num ** 14)) + '''
| ''' + str(format_float(num ** 15)) + '''
| ''' + str(format_float(num ** 16)) + '''
| ''' + str(format_float(num ** 17)) + '''
| ''' + str(format_float(num ** 18)) + '''
| ''' + str(format_float(num ** 19)) + '''
| ''' + str(format_float(num ** 20)) + '''
|-
| x<sup>''' + str(num) + '''</sup>
| ''' + str(format_float(1 ** num)) + '''
| ''' + str(format_float(2 ** num)) + '''
| ''' + str(format_float(3 ** num)) + '''
| ''' + str(format_float(4 ** num)) + '''
| ''' + str(format_float(5 ** num)) + '''
| ''' + str(format_float(6 ** num)) + '''
| ''' + str(format_float(7 ** num)) + '''
| ''' + str(format_float(8 ** num)) + '''
| ''' + str(format_float(9 ** num)) + '''
| ''' + str(format_float(10 ** num)) + '''
!
| ''' + str(format_float(11 ** num)) + '''
| ''' + str(format_float(12 ** num)) + '''
| ''' + str(format_float(13 ** num)) + '''
| ''' + str(format_float(14 ** num)) + '''
| ''' + str(format_float(15 ** num)) + '''
| ''' + str(format_float(16 ** num)) + '''
| ''' + str(format_float(17 ** num)) + '''
| ''' + str(format_float(18 ** num)) + '''
| ''' + str(format_float(19 ** num)) + '''
| ''' + str(format_float(20 ** num)) + '''
|}'''

    return output


def run(site):
    print("Running skep_getalle")
    current_num = 13
    page_title = str(current_num) + " (getal)"
    page = pywikibot.Page(site, page_title)

    if page.exists():
        print('\n' + page_title + ' bestaan reeds.\n')
        quit()
    else:
        print("Besig om " + page_title + " te skep")

    intro = ("'''" + str(current_num) + "''' is 'n [[natuurlike getal]] wat ná [["
             + str((current_num - 1)) + " (getal)|" + str((current_num - 1)) + "]] en vóór [[" + str((current_num + 1))
             + " (getal)|" + str((current_num + 1)) + "]] op die getallelyn lê.")

    is_prime = is_prime_number(current_num)
    prime_text = ""
    if is_prime:
        nth_prime = ""  # todo
        prime_text = str(current_num) + " is 'n [[priemgetal]]."

    even_text = ""
    if is_even_number(current_num):
        even_text = "'n [[Heelgetal]] word as 'n [[Pariteit|ewe getal]] bepaal as dit deelbaar deur [[2 (getal)|twee]] is. Wanneer dit in basis 10 geskryf word, sal alle veelvoude van 2 eindig op 0, 2, 4, 6 of 8.<ref name=\"oeis\">{{Cite web|url=https://oeis.org/A005843 |title=The On-Line Encyclopedia of Integer Sequences. |access-date=2022-12-15 |lang=en}}</ref>"

    calc_table = getCalcString(current_num)

    # Define a template string with a placeholder
    template = Template(''':''Hierdie artikel handel oor die getal $current_num, vir ander gebruike, sien [[$current_num (dubbelsinnig)]].''
{{getal
| simbool = $current_num
| orde =<!--b.v. eerste, tweede, derde  | net een -->
}}

$intro $prime_text

== Wiskunde ==
$even_text

$calc_table

== Verwysings ==
{{Verwysings}}

== Eksterne skakels ==
* {{Commons category-inline|$current_num (number}}

{{Normdata}}
[[Kategorie:Getalle]]''')

    # Create a dictionary with the values to substitute
    values = {'current_num': current_num, 'title': page_title, 'intro': intro, 'calc_table': calc_table,
              'prime_text': prime_text, 'even_text': even_text}

    # Use the substitute method to replace the placeholders with actual values
    filled_template = template.substitute(values)

    page.text = filled_template
    # print(page.text)

    page.save('Nuwe bladsy geskep - ' + page_title, minor=False)
