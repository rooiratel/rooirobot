#!/usr/bin/env python
import csv
import os

import pywikibot
import httpx
from bs4 import BeautifulSoup

bs_parser = 'html.parser'


def get_taxonomic_family(soup):
    tax_fam_sel = soup.select_one(
        'div.taxon-header div.taxon-header__summary div.c-summary div.taxon-classification div.taxon-classification__item span a.teal-link em')

    return tax_fam_sel.text


def get_synonyms_list(soup):
    clean_syn_list = []

    syn_section = soup.select_one('section#synonyms')
    if syn_section is None:
        print("No synonyms for this species")
        return clean_syn_list
    else:

        syn_list = syn_section.select(
            'div.row div.taxon-section__content ul.c-synonym-list li.p-regular a.teal-link')

        cleaner_syn_list = [tag.text for tag in syn_list]

        print('found ' + str(len(syn_list)) + ' synonyms in list')

        for syn in cleaner_syn_list:
            s = str(syn)
            s = s.replace('<em lang="la">', '')
            s = s.replace('</em>', '')
            s = s.replace('\n', '')
            s = s.strip()

            s = surround_auths_with_smol_tags(s)

            clean_syn_list.append(s)

        return clean_syn_list


def surround_auths_with_smol_tags(auth_str):
    # Put authorities in <small> tags
    small_tag = '<small>'

    # Split the string into tokens
    tokens = auth_str.split()

    # Handle var. and f.
    for i, token in enumerate(tokens):
        if token == 'var.' or token == 'f.' or token == 'subsp.':
            tokens[i + 2] = small_tag + tokens[i + 2]
            return ' '.join(tokens) + '</small>'

    # Find the first token that contains the '(' character
    for i, token in enumerate(tokens):
        if '(' in token:
            tokens[i] = small_tag + token
            return ' '.join(tokens) + '</small>'

    # Find the first token that contains the '.' character
    for i, token in enumerate(tokens):
        if '.' in token and token != 'var.' and token != 'f.':
            tokens[i] = small_tag + token
            return ' '.join(tokens) + '</small>'

    # If there are exactly 3 tokens, prepend the last token with '<small>'
    # if len(tokens) == 3:
    #     tokens[-1] = small_tag + tokens[-1]

    # If there is not already a small token, then prepend it to the last token
    if small_tag not in tokens:
        tokens[-1] = small_tag + tokens[-1]

    # Join the token back together into a string
    return ' '.join(tokens) + '</small>'


def create_csv_from_url(soup):
    cur_dir = os.path.dirname(__file__)
    filename = os.path.join(cur_dir, 'continuous/species_list.csv')

    if not os.path.exists(filename):
        print(filename + ' does not exist. Creating it')
    else:
        print(filename + ' exists. Overwriting it')

    with open(filename, 'w') as file:
        file.write('genus,species,authority,source\n')  # csv header line

    # get list of species
    species_section = soup.select_one('section#children')

    species_list = species_section.select(
        'div.row div.taxon-section__content ul.c-synonym-list li.p-regular a.teal-link')

    print('found ' + str(len(species_list)) + ' species in list')

    url_prefix = 'https://powo.science.kew.org'

    # append to the file
    with open(filename, "a") as f:
        for sp in species_list:
            taxa = sp.select_one('em').text

            if len(taxa.split()) == 1:
                # skip hybrids
                continue

            csv_genus = taxa.split()[0]
            csv_species = taxa.split()[1]
            csv_authority = sp.text.replace(taxa, '').strip()
            csv_authority = csv_authority.replace(',', ' en ').strip()  # does not work?
            csv_source = url_prefix + sp.get('href')

            f.write(csv_genus + ',' + csv_species + ',' + csv_authority + ',' + csv_source + '\n')


def link_page_to_wikidata(page_name):
    wiki_list = ['ceb', 'vi', 'en', 'se', 'pt', 'war']

    for wiki in wiki_list:

        site = pywikibot.Site(wiki, "wikipedia")
        page = pywikibot.Page(site, page_name)

        try:
            item = pywikibot.ItemPage.fromPage(page)

            print("found wikidata item for : " + wiki)

            # Get the Wikidata item ID
            wikidata_id = item.getID()

            wd_site = pywikibot.Site('wikidata', 'wikidata')

            item = pywikibot.ItemPage(wd_site, wikidata_id)

            # Set the sitelink
            item.setSitelink({'site': 'afwiki', 'title': page_name}, summary=page_name)

            # Set the language label
            item.editLabels({'af': page_name}, summary=page_name)

            # Set the description
            item.editDescriptions({'af': 'plantspesie'}, summary='plantspesie')
            print('Bladsy is nou gekoppel aan wikidata')
            break

        except pywikibot.exceptions.NoPageError:
            print("No corresponding Wikidata item found for " + wiki)
            continue


def get_native_distribution_list(soup):
    dist_section = soup.select_one('section#distributions')

    # using .select_one() excluded the "Introduced" areas
    dist_div = dist_section.select_one('div.row div.col-12 div.taxon-section__content div#distribution-listing p.p')

    print("defucking string")
    clean_div_text = dist_div.text.strip()
    clean_div_text = clean_div_text.replace('  ', ' ').replace('\n', '').replace('\t', '').replace('\r', '').replace(
        '\f', '').replace('\v', '')

    while ',  ' in clean_div_text:
        clean_div_text = clean_div_text.replace(',  ', ', ')

    # now loop over each item in the list and check if it is in the translation list
    # append if not in the list
    append_to_translate_list(clean_div_text)

    return translate_dist_list(clean_div_text)


def append_to_translate_list(dist_str):
    dist_list = dist_str.split(',')
    cur_dir = os.path.dirname(__file__)
    f_path = os.path.join(cur_dir, 'place_name_translate.csv')

    with open(f_path, 'r') as f:
        rdr = csv.reader(f)
        first_column = [r[0] for r in rdr]

        for region in dist_list:
            region = region.strip().replace('\n', '')
            if region not in first_column:
                translated_region = input('What is the Afrikaans name for : ' + region + ' ?\n')
                translated_region = translated_region.strip().replace('\n', '')

                with open(f_path, 'a') as fa:
                    writer = csv.writer(fa, quoting=csv.QUOTE_NONE)
                    writer.writerow([region, '[[' + translated_region + ']]'])


def translate_dist_list(dist_list):
    print("translating dist_list")
    cur_dir = os.path.dirname(__file__)
    f_path = os.path.join(cur_dir, 'place_name_translate.csv')

    # Read the CSV file
    with open(f_path, 'r') as file:
        tdl_reader = csv.reader(file)
        next(tdl_reader)  # Skip the header row
        replacements = {tdl_row[0]: tdl_row[1] for tdl_row in tdl_reader}

    # Replace substrings in the input string
    for old, new in replacements.items():
        dist_list = dist_list.replace(old, new)

    return dist_list


def run(site, genus_powo_link):
    print("Begin")

    powo_link = genus_powo_link  # 'https://powo.science.kew.org/taxon/urn:lsid:ipni.org:names:330561-2'

    with httpx.Client() as client:
        response = client.get(powo_link)

    soup = BeautifulSoup(response.content, bs_parser)
    # pass above into below method
    create_csv_from_url(soup)

    # parent_taxon = input("Name of taxonomic family that this species belongs to:")

    curr_dir = os.path.dirname(__file__)
    file_path = os.path.join(curr_dir, 'continuous/species_list.csv')

    with open(file_path) as csvfile:
        reader = csv.DictReader(csvfile)
        with httpx.Client() as client2:
            for row in reader:
                genus = row['genus']
                species = row['species']
                authority = row['authority']
                descriptor = 'plant'
                source = row['source']

                source_response = client2.get(source)
                source_soup = BeautifulSoup(source_response.content, bs_parser)

                parent_taxon = get_taxonomic_family(source_soup)
                parent_taxon_type = "[[Familie (biologie)|familie]]"

                page_title = genus + " " + species
                page = pywikibot.Page(site, page_title)

                if page.exists():
                    print('\n' + page_title + ' bestaan reeds. Slaan oor na die volgende bladsy.\n')
                    continue

                page.text = ''
                page.text += '{{Spesieboks\n'

                page.text += '| name = <!-- Afrikaanse naam -->\n'
                page.text += '| status = \n'
                page.text += '| status_system = iucn3.1\n'
                page.text += '| status_ref = \n'
                page.text += '| image = \n'
                # page.text += '| genus = ' + genus + ' (plant) \n'
                page.text += '| taxon = ' + genus + ' ' + species + '\n'
                # page.text += '| genus = ' + genus + '\n'
                # page.text += '| species = ' + species + '\n'
                page.text += '| authority = ' + authority + '\n'

                syn_lys = get_synonyms_list(source_soup)
                if len(syn_lys) > 0:

                    page.text += '| synonyms_ref = <ref name="powo" />\n'
                    page.text += '| synonyms = {{List collapsed\n   | title = Lys van sinonieme\n   | bullets=on\n'

                    for synonym in syn_lys:
                        page.text += '   | ' + str(synonym) + '\n'

                    page.text += '  }}\n'

                page.text += '}}\n'
                page.text += "'''''" + page_title + "''''' is 'n [[" + descriptor + "]] wat deel is van die [[" + parent_taxon + "]] " + parent_taxon_type + '.<ref name="powo" />\n\n'

                page.text += '== Verspreiding ==\n'
                page.text += 'Die spesie is [[Inheemse plant|inheems]] aan ' + get_native_distribution_list(
                    source_soup) + '.<ref name="powo" />\n'

                page.text += '\n<!--\n'
                page.text += '== Beskrywing ==\n'
                page.text += '== Bronnelys ==\n'
                page.text += '-->\n'
                page.text += '== Verwysings ==\n'
                page.text += '{{Verwysings|verwysings=\n'
                page.text += '<ref name="powo">{{cite web |url=' + source + ' |title=' + page_title + ' op POWO}}</ref>\n'
                page.text += '}}\n\n'

                page.text += '{{Saadjie}}\n{{Taksonbalk}}\n'
                page.text += '[[Kategorie:' + genus + '|' + species + ']]'

                # print(page.text)

                page.save('Nuwe bladsy geskep - ' + page_title, minor=False)

                print("Probeer om aan wikidata te skakel")
                link_page_to_wikidata(page_title)
                quit()  # This makes it only run the first item then exit for debugging

    # NOTE : https://www.wikidata.org/wiki/Wikidata:Pywikibot_-_Python_3_Tutorial/Setting_sitelinks

# TODO : add SANBI source
# TODO : add authority date
# TODO : status (EN, CR etc.)
# TODO : add to category for each country (e.g. Flora of Brazil)
# TODO : get common names from inat
