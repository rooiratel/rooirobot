#!/usr/bin/env python
import pywikibot


def run(page_list, site):
    print("Running taxon_category_fmt")
    debug = False

    for page in page_list:
        page = pywikibot.Page(site, page)
        page_title = page.title()
        text = page.text

        if not page.exists():
            print('Bladsy :' + page_title + ' bestaan nie.')
        else:
            print('Besig met : ' + page_title)

            split_title = page_title.split(' ')
            genus = split_title[0]
            species = split_title[1]

            correct_kat_str = '[[Kategorie:' + genus + '|' + species + ']]'
            wrong_kat_str = '[[Kategorie:' + genus + ']]'

            if wrong_kat_str in text:
                print("- " + wrong_kat_str)
                text = text.replace(wrong_kat_str, '').rstrip('\n')

            if correct_kat_str in text:
                print("Kategorie reeds korrek")
            else:
                print('+ ' + correct_kat_str)
                text = text + '\n' + correct_kat_str

            # remove blank lines
            while '\n\n[[Kategorie' in text:
                text = text.replace('\n\n[[Kategorie', '\n[[Kategorie')

            page.text = text

            if debug:
                print(page.text)
            else:
                page.save('Kategorie fmt', minor=True)
