== Inligting oor die roete ==
* Lengte: {{Omreken|258.9|km|0}}
* Oorsprong: [[Atsuta-ku, Nagoya]]
* Eindpunt: [[Nagano]]
* Hoof stede op die roete: Kasugai, Tajimi, Nakatsugawa, Shiojiri, Matsumoto, Azumino

== Beelde ==
<gallery>



</gallery>

Lêer:example|

======
js
======
var imgs = document.getElementsByClassName('image');
var i;
var out = '';
for (i = 0; i < imgs.length; ++i) { 
    var file = imgs[i].getAttribute("href");
    out += file.replace("/wiki/File","Lêer");
    out += "|\n"
}
alert(out);
