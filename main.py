import pywikibot

import scripts.change_to_spesieboks
import scripts.taxon_category_fmt
import scripts.cleanup
import scripts.sb_fix
import scripts.text_replace
import scripts.wees_status_nagaan
import scripts.voorbladartikelkandidate
import scripts.skep_film
import scripts.skep_takson_csv
import scripts.skep_getalle
import scripts.voeg_beeld_by
import scripts.mylpale
import scripts.kopblad
import scripts.skep_wikidata_takson


def get_list_of_random_pages(site, limit) -> list:
    # get a random number of pages within a range:
    list_of_random_pages = []
    counter = 0

    while counter <= limit:
        print(f"\rFound {counter} / {limit} pages", end='', flush=True)

        # get random page
        for page in site.randompages(1, 0, False, False):
            list_of_random_pages.append(page.title())
            counter = counter + 1
    print("")
    return list_of_random_pages


def get_list_of_pages_from_file() -> list:
    # Load a list of pages to edit/create from a text file
    f = open('list_of_pages.txt', 'r')
    return f.readlines()


# This is the main file

# Setup
site = pywikibot.Site("af", "wikipedia")

# list_of_pages = ["Jeremy Loops", "Hermann Picha"]
list_of_pages = get_list_of_random_pages(site, 100)
# list_of_pages = get_list_of_pages_from_file()

# Call the functions you want to run on each page
# It is up to each individual function on what must happen if a page does or doesn't exist

# scripts.cleanup.run(list_of_pages, site)
# scripts.change_to_spesieboks.run(list_of_pages, site)
# scripts.taxon_category_fmt.run(list_of_pages, site)
# scripts.sb_fix.run(list_of_pages, site)
# scripts.mylpale.run(list_of_pages, site)
# scripts.skep_wikidata_takson.run("Schrebera trifoliata", "Q9075158", "77326985-1", "", "")
# scripts.kopblad.run(list_of_pages, site)
# scripts.voorbladartikelkandidate.run(site)
scripts.wees_status_nagaan.run(list_of_pages, site)
# scripts.text_replace.run(list_of_pages, site, "''Requiem for a Dream''", "''[[Requiem for a Dream]]''")
# scripts.skep_film.run(site, 'alien_romulus', 'tt18412256', '', "Alien: Romulus", False)
# scripts.skep_getalle.run(site)
# scripts.skep_takson_csv.run(site, 'https://powo.science.kew.org/taxon/urn:lsid:ipni.org:names:330561-2')
# scripts.voeg_beeld_by.run(site, "Nordiese Afrikainstituut", "Nordiska Afrikainstitutet (The Nordic Africa Institute).jpg", "Die Nordiese Afrikainstituut in Uppsala, Swede")
